<?php
require_once __DIR__ . '/../vendor/autoload.php';

$loader = new Twig_Loader_Filesystem(__DIR__."/../templates");
$twig = new Twig_Environment($loader, [
//    'cache' => __DIR__ . '/../tmp/cache',
    'cache' => false
]);

$database = new \App\model\Database();

$router = new \App\Router($database, $twig);
if (preg_match('/\.(?:css|png|whatever)$/', $_SERVER["REQUEST_URI"] ))
{
    return false;
} else {
    echo $router->process($_GET);
}
