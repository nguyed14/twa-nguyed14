<?php

namespace App;

use App\Model\Database;
use Twig_Error;

class Router
{
	/** @var Database */
	protected $database;

	/** @var \Twig_Environment */
	protected $twig;

	/**
	 * Router constructor.
	 * @param Database $database
	 * @param \Twig_Environment $twig
	 */
	public function __construct ( Database $database, \Twig_Environment $twig )
	{
		$this->database = $database;
		$this->twig = $twig;
	}

	public function process ( $parameters ): string
	{
		$page = $this->getParameter($parameters, 'page', 'index');

		switch ( $page ) {
			case 'index':
				return $this->routeIndex();
			case 'detail':
				$id = $this->getParameter($parameters, 'id');
				return $this->routeDetail($id);
			case 'edit':
				$id = $this->getParameter($parameters, 'id');
				return $this->routeEdit($id);
			default:
				return $this->routeNotFound();
		}
	}

	protected function getParameter ( $array, $key, $default = null )
	{
		if ( array_key_exists($key, $array) )
			return $array[ $key ];
		return $default;
	}

	protected function routeIndex (): string
	{
		try {
			$template = $this->twig->load('index.html.twig');
			return $template->render([ 'employees' => $this->database->getEmployees() ]);
		} catch ( Twig_Error $e ) {
			return $this->routeInternalServerError($e);
		}
	}

	protected function routeInternalServerError ( \Throwable $ex ): string
	{
		try {
			$template = $this->twig->load('500.html.twig');
			http_response_code(500);
			return $template->render(['ex' => $ex]);
		} catch ( Twig_Error $e ) {
			http_response_code(500);
			return '500 Internal Server Error';
		}
	}

	protected function routeDetail ( $id ): string
	{
		if ( ($employee = $this->database->getEmployee($id)) === null ) {
			return $this->routeNotFound();
		}

		try {
			$template = $this->twig->load('detail.html.twig');
			return $template->render([ 'employee' => $employee ]);
		} catch ( Twig_Error $e ) {
			return $this->routeInternalServerError($e);
		}
	}

    protected function routeEdit ( $id ): string
    {
        if ( ($employee = $this->database->getEmployee($id)) === null ) {
            return $this->routeNotFound();
        }

        try {
            $template = $this->twig->load('edit.html.twig');
            return $template->render([ 'employee' => $employee,
                'positions' => ['User', 'Super Admin', 'Developer', 'Tester', 'Admin']]);
        } catch ( Twig_Error $e ) {
            return $this->routeInternalServerError($e);
        }
    }

	protected function routeNotFound (): string
	{
		try {
			$template = $this->twig->load('404.html.twig');
			http_response_code(404);
			return $template->render();
		} catch ( Twig_Error $e ) {
			return $this->routeInternalServerError($e);
		}
	}
}
