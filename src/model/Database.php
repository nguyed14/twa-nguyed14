<?php

namespace App\model;

class Database{
	/** @var Employee[] */
	protected $employees = [];

	const POSITIONS = [
	    [
	        'Admin', 'User', 'Developer'
        ],
        [
            'User', 'Super Admin', 'Developer', 'Tester'
        ],
        [
            'Super Admin', 'User', 'Developer'
        ],
        [
            'Developer'
        ],
        [
            'Tester'
        ]
    ];

	const PHONENUMBER = [
	    '14123124', '12121455', '121312312',
        '12312312', '12312412', '1215676', '56675856'
    ];

	const TEXT = [
	    'Prepared do an dissuade be so whatever steepest. Yet her beyond looked either day wished nay. By doubtful disposed do juvenile an. Now curiosity you explained immediate why behaviour. An dispatched impossible of of melancholy favourable. Our quiet not heart along scale sense timed. ',
        'Consulted he eagerness unfeeling deficient existence of. Calling nothing end fertile for venture way boy. Esteem spirit temper too say adieus who direct esteem. It esteems luckily mr or picture placing drawing no. Apartments frequently or motionless on reasonable projecting expression. Way mrs end gave tall walk fact bed. ',
        'On then sake home is am leaf. Of suspicion do departure at extremely he believing. Do know said mind do rent they oh hope of. General enquire picture letters garrets on offices of no on. Say one hearing between excited evening all inhabit thought you. Style begin mr heard by in music tried do. To unreserved projection no introduced invitation. ',
        'On no twenty spring of in esteem spirit likely estate. Continue new you declared differed learning bringing honoured. At mean mind so upon they rent am walk. Shortly am waiting inhabit smiling he chiefly of in. Lain tore time gone him his dear sure. Fat decisively estimating affronting assistance not. Resolve pursuit regular so calling me. West he plan girl been my then up no. ',
        'Barton did feebly change man she afford square add. Want eyes by neat so just must. Past draw tall up face show rent oh mr. Required is debating extended wondered as do. New get described applauded incommode shameless out extremity but. Resembled at perpetual no believing is otherwise sportsman. Is do he dispatched cultivated travelling astonished. Melancholy am considered possession on collecting everything. '
    ];

	private function getRandom(int $min, int $max) : int{
	    try{
	        $number = random_int($min, $max);
        }catch(\Exception $e){
            return null;
        }

        return $number;
    }

	/**
	 * Database constructor.
	 * @param Employee[] $employees
	 */
	public function __construct (){
		$this->employees[] = Employee::create(13333, 'Pavlik', 'Kundera', 'pkund@seznam.cz', 'https://i.pravatar.cc/50?img=1')
            ->setPhoneNumber( self::PHONENUMBER[self::getRandom(0, 6)])
            ->setPositions(self::POSITIONS[self::getRandom(0, 4)])
            ->setAbout(self::TEXT[self::getRandom(0, 4)]);
        $this->employees[] = Employee::create(2, 'Pav', 'Kudera', 'pkude@seznam.cz', 'https://i.pravatar.cc/50?img=2')
            ->setPhoneNumber( self::PHONENUMBER[self::getRandom(0, 6)])
            ->setPositions(self::POSITIONS[self::getRandom(0, 4)])
            ->setAbout(self::TEXT[self::getRandom(0, 4)]);
        $this->employees[] = Employee::create(3, 'Pavlik', 'Kund', 'pakund@seznam.cz', 'https://i.pravatar.cc/50?img=3')
            ->setPhoneNumber( self::PHONENUMBER[self::getRandom(0, 6)])
            ->setPositions(self::POSITIONS[self::getRandom(0, 4)])
            ->setAbout(self::TEXT[self::getRandom(0, 4)]);
        $this->employees[] = Employee::create(4, 'Paik', 'Kundera', 'paikund@seznam.cz', 'https://i.pravatar.cc/50?img=4')
            ->setPhoneNumber( self::PHONENUMBER[self::getRandom(0, 6)])
            ->setPositions(self::POSITIONS[self::getRandom(0, 4)])
            ->setAbout(self::TEXT[self::getRandom(0, 4)]);
        $this->employees[] = Employee::create(5, 'Pavk', 'Dera', 'pde@seznam.cz', 'https://i.pravatar.cc/50?img=5')
            ->setPhoneNumber( self::PHONENUMBER[self::getRandom(0, 6)])
            ->setPositions(self::POSITIONS[self::getRandom(0, 4)])
            ->setAbout(self::TEXT[self::getRandom(0, 4)]);
        $this->employees[] = Employee::create(6, 'Vlik', 'Kundera', 'vkund@seznam.cz', 'https://i.pravatar.cc/50?img=6')
            ->setPhoneNumber( self::PHONENUMBER[self::getRandom(0, 6)])
            ->setPositions(self::POSITIONS[self::getRandom(0, 4)])
            ->setAbout(self::TEXT[self::getRandom(0, 4)]);
        $this->employees[] = Employee::create(7, 'Pe', 'Kund', 'pekund@seznam.cz', 'https://i.pravatar.cc/50?img=7')
            ->setPhoneNumber( self::PHONENUMBER[self::getRandom(0, 6)])
            ->setPositions(self::POSITIONS[self::getRandom(0, 4)])
            ->setAbout(self::TEXT[self::getRandom(0, 4)]);
	}

    /**
     * @return Employee[]
     */
    public function getEmployees(): array
    {
        return $this->employees;
    }

	/**
	 * @param $id
	 * @return Employee|null
	 */
	public function getEmployee ( $id ): ?Employee
	{
		foreach ( $this->employees as $employee )
			if ( $employee->getId() == $id )
				return $employee;
		return null;
	}
}
