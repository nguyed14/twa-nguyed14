<?php

namespace App\model;

Class Employee{
    /** @var string */
    protected $surname;

    /** @var string */
    protected $forename;

    /** @var string */
    protected $email;

    /** @var int */
    protected $id;

    /** @var string */
    protected $linkPhoto;

    /** @var string */
    protected $phoneNumber;

    protected $positions = [];

    protected $about;

    /**
     * Employee constructor.
     * @param string $surname
     * @param string $forename
     * @param string $email
     * @param int $id
     * @param string $linkPhoto
     */
    public function __construct($id, $surname, $forename, $email, $linkPhoto)
    {
        $this->id = $id;
        $this->surname = $surname;
        $this->forename = $forename;
        $this->email = $email;
        $this->linkPhoto = $linkPhoto;
    }

    /**
     * @param int $id
     * @param string $surname
     * @param string $forename
     * @param string $email
     * @param string $linkPhoto
     * @return Employee
     */
    public static function create ( int $id, string $surname, string $forename, string $email, string $linkPhoto) : Employee
    {
        return new static($id, $surname, $forename, $email, $linkPhoto);
    }

    /**
     * @param mixed $about
     * @return $this
     */
    public function setAbout($about): Employee
    {
        $this->about = $about;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @return array
     */
    public function getPositions(): array
    {
        return $this->positions;
    }

    /**
     * @return mixed
     */
    public function getAbout()
    {
        return $this->about;
    }



    /**
     * @param array $positions
     * @return $this
     */
    public function setPositions(array $positions): Employee
    {
        $this->positions = $positions;
        return $this;
    }

    /**
     * @param string $phoneNumber
     * @return $this
     */
    public function setPhoneNumber(string $phoneNumber): Employee
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getForename(): string
    {
        return $this->forename;
    }

    /**
     * @param string $forename
     */
    public function setForename(string $forename): void
    {
        $this->forename = $forename;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getLinkPhoto(): string
    {
        return $this->linkPhoto;
    }

    /**
     * @param string $linkPhoto
     */
    public function setLinkPhoto(string $linkPhoto): void
    {
        $this->linkPhoto = $linkPhoto;
    }
}